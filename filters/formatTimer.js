angular.module('timeTrackerApp').filter('formatTimer', function() {
    return function(input)
    {
        function z(n) {return (n<10? '0' : '') + n;}
        var seconds = input % 60;
        var minutes = Math.floor(input / 60);
        var hours = Math.floor(minutes / 60);

     //   console.log('hrs',hours);

        if(minutes < 60 && hours == 0) {
            return (z(minutes) + ':' + z(seconds));
        }
        else {
            return (z(hours) + ':' + z(minutes) + ':' + z(seconds));
        }
    };
});