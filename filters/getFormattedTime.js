angular.module('timeTrackerApp').filter('getAmPm', function() {
    //  console.log(data);
    return function(inputTime) {

        var time_part_array = inputTime.split(":");
        var ampm = 'AM';

        if (time_part_array[0] >= 12) {
            ampm = 'PM';
        }

        if (time_part_array[0] > 12) {
            time_part_array[0] = time_part_array[0] - 12;
        }

     //   formatted_time = time_part_array[0] + ':' + time_part_array[1] + ':' + time_part_array[2] + ' ' + ampm;

        formatted_time = time_part_array[0] + ':' + time_part_array[1] + ' ' + ampm;

        return formatted_time;

     //   return (!!initialChar) ? initialChar.charAt(0).toUpperCase() : '';
    }
});