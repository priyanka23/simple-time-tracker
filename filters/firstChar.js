angular.module('timeTrackerApp').filter('firstChar', function() {
  //  console.log(data);
    return function(initialChar) {
        return (!!initialChar) ? initialChar.charAt(0).toUpperCase() : '';
    }
});