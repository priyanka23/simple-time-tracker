angular.module('timeTrackerApp').controller('ManualEntryController',[
    '$scope','$http','$cookies','$location',function ($scope, $http, $cookies, $location) {

        $scope.manual_entry = {};

        $scope.manual_entry.projectId = "";

        $scope.get_projects = function () {
            var user_id = $cookies.getObject('user');
            $http.get('api/projects/'+user_id.user.id).then(function (result) {

                 console.log(result.data);
                 $scope.projects = result.data;

            });
        };

        $scope.get_projects();


        $scope.getTasks = function (project_id) {
            console.log(project_id);

            $http.get('api/get-project-tasks/'+project_id).then(function (result) {

               console.log(result.data);
               $scope.tasks = result.data;

            });
        };


        $scope.save_entry = function() {
           console.log($scope.manual_entry);

                $http.post('api/task-log-entry', $scope.manual_entry).then(function (result) {

                    console.log(result);
                      if(result.data.status === 200) {
                        $scope.message = "Project added successfully";
                        $scope.manual_entry = {};


                     }

                });

        };

        $scope.checkTime = function () {
           console.log($scope.manual_entry.startTime);
           console.log($scope.manual_entry.endTime);

            var time1 = $scope.manual_entry.startTime.getTime();
            var time2 = $scope.manual_entry.endTime.getTime();

            console.log('time 1',time1);
            console.log('time 2',time2);

            var date1 = new Date(time1);
            var date2 = new Date(time2);


            var differenceTravel = time2 - time1;
            var diff_seconds = Math.floor((differenceTravel) / (1000));

            var diff_mins = Math.floor((differenceTravel) / (1000*60));
            console.log('diff mins =',diff_mins);



            if(diff_seconds < 0) {
                alert('End time should be greater than start time');
            }
            else {
                if (diff_seconds < 60) {
                    alert('Task duration must be equal or greater to 1 mins');
                }
                else {
                    var hrs1 = date1.getHours();
                    var hrs2 = date2.getHours();

                    var mins1 = date1.getMinutes();
                    var mins2 = date2.getMinutes();


                    var seconds1 = date1.getSeconds();
                    var seconds2 = date2.getSeconds();

                    if (seconds1 < 10) {
                        seconds1 = '0' + seconds1;
                    }

                    if (seconds2 < 10) {
                        seconds2 = '0' + seconds2;
                    }

                    if (mins1 < 10) {
                        mins1 = '0' + mins1;
                    }

                    if (mins2 < 10) {
                        mins2 = '0' + mins2;
                    }

                    if (hrs1 < 10) {
                        hrs1 = '0' + hrs1;
                    }

                    if (hrs2 < 10) {
                        hrs2 = '0' + hrs2;
                    }


                    $scope.manual_entry.start_time = hrs1 + ':' + mins1 + ":" + seconds1;
                    $scope.manual_entry.end_time = hrs2 + ':' + mins2 + ":" + seconds2;

                    $scope.manual_entry.time_diff_minutes = diff_mins;


                    console.log('diff_seconds = ', diff_seconds);
                }
            }

        };








    }
]);
