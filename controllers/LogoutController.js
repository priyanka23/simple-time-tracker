angular.module('timeTrackerApp').controller('LogoutController',['$scope', '$rootScope', '$cookies' ,'$http', '$location', function($scope, $rootScope, $cookies ,$http, $location ) {

        $rootScope.authenticated = false;

        $scope.init = function () {

            var expire = new Date();
            expire.setDate(expire.getDate() - 1);
            $cookies.putObject('user_stt', $rootScope.authenticated.user, {expires: expire, 'path': '/'});
            $cookies.remove('user_stt');
            $location.path('/login');

        };

        $scope.init();

    }]);