angular.module('timeTrackerApp').controller ('ManageProjectController',[
    '$scope','$http','$location','$route','$cookies', 'utility','SweetAlert',
    function ($scope, $http,$location,$route,$cookies, utility, SweetAlert) {


        $scope.data = {};

        $scope.pro = {};
        $scope.pro.project_task = "";

        $scope.rename = {};


        $scope.rename_proj = {};
       // $scope.data.project= {};
       // $scope.data.project.color_class = '';

        $scope.colors = [
            {
                color_code: '#5700F1',
                color_class: 'c1',
                task_code:'task_background_1'
            },
            {
                color_code: '#FF085C',
                color_class: 'c2',
                task_code:'task_background_2'
            },
            {
                color_code: '#1597FF',
                color_class: 'c3',
                task_code:'task_background_3'
            },
            {
                color_code: '#00AB09',
                color_class: 'c4',
                task_code:'task_background_4'
            },
            {
                color_code: '#FB8B00',
                color_class: 'c5',
                task_code:'task_background_5'
            },
            {
                color_code: '#4E342E',
                color_class: 'c6',
                task_code:'task_background_6'
            },
            {
                color_code: '#FF3333',
                color_class: 'c7',
                task_code:'task_background_7'
            },
            {
                color_code:'#0025F4',
                color_class: 'c8',
                task_code:'task_background_8'
            }];


        $scope.get_project_data = function () {

            var user_id = $cookies.getObject('user_stt');
            $http.get('api/projects/'+user_id.user.id).then(function (result) {

                // console.log(result.data);
                $scope.projects = result.data;
            });
        };

        $scope.get_project_data();

        $scope.click_me = function ($event) {
            $event.preventDefault();
            $('#modal1').openModal();
            console.log(1);

        };

        $scope.applyClass = function (color) {
      //      console.log('color', color);
       //     console.log (utility.findObjectIndex($scope.colors, 'color_code', color), color);
            var index = utility.findObjectIndex($scope.colors, 'color_code', color);
            return $scope.colors[index].color_class;

        };

        $scope.editProject= function (project) {
           // console.log(project);
            $scope.data.project = project;
            console.log( $scope.data.project);
        };


        $scope.addSingleTask = function (task_name, project_id) {
            console.log('task',task_name);
            console.log('project_id ',project_id);

            //  $scope.task = [];

            $scope.task =  {
                task_name: task_name,
                project_id: project_id
            };

            console.log($scope.task);


            $http.post('api/add-task',$scope.task).then(function (result) {

                console.log(result);
                if(result.data.status === 200) {
                    $scope.message = "Task added successfully";

                    $scope.pro.project_task = "";
                    $scope.pro = {};
                    $scope.get_project_data();

                    $http.get('api/get-project-tasks/'+$scope.data.project.id).then(function (result) {
                        console.log(result.data);
                        $scope.data.project.tasks = result.data;
                    });

                   // $scope.


                 //   $scope.data = {};
                  //    $scope.taskAddForm.$setUntouched();
                }

            });

        };



       //   open data from the first project on load of a page *
/*
        $scope.init = function () {
            $scope.get_project_data();
            $scope.data.project = $scope.projects[0];
        };

        $scope.init();
*/





       $scope.rename_project = function (){
           $scope.rename_proj.project_name = $scope.data.project.project_name;
           $scope.rename_proj.project_id = $scope.data.project.id;
           $('#rename_modal').openModal();
       };

        $scope.update_name = function () {



            console.log('rename', $scope.rename_proj );
         //  console.log($scope.rename.project_name);

            $http.post('api/update-project',$scope.rename_proj).then(function(result){

                if(result.data.status === 200) {

                    $scope.data.project.project_name = result.data.project_name;

                    $scope.get_project_data();
                }


            });
        };

        $scope.delete_project = function () {
          // console.log($scope.data.project.id);

            var index = utility.findObjectIndex($scope.projects, 'id', $scope.data.project.id);

           // console.log ('pro index= ',index);

            SweetAlert.swal({
             title: "Are you sure want to delete " + $scope.data.project.project_name + "?", //Bold text
             type: "warning", //type -- adds appropiriate icon
             showCancelButton: true, // displays cancel btton
             confirmButtonColor: "#DD6B55",
             confirmButtonText: "Yes, delete it!",
             closeOnConfirm: true, //do not close popup after click on confirm, usefull when you want to display a subsequent popup
             closeOnCancel: true
             },
             function (isConfirm) { //Function that triggers on user action.
             if (isConfirm) {

             $scope.delete_pro = {};
             $scope.delete_pro.id = $scope.data.project.id;

             $http.post('api/delete-project',$scope.delete_pro).then(function(result){

             if(result.data.status === 200) {

                $scope.get_project_data();

                 console.log($scope.projects);

                     // newVal is defined
                 $scope.projects.splice(index, 1);

              //   console.log('def',angular.isDefined($scope.projects[index]));

                 if(angular.isDefined($scope.projects[index])) {

                     // load next project of current project in the view
                     $scope.data.project = $scope.projects[index];

                 }
                 else {
                     // load first project in the view if its the only project of the list
                     $scope.data.project = $scope.projects[0];


                     //   $scope.data.project = $scope.projects[0];

                 }

         //     SweetAlert.swal("Deleted!");

             //   $scope.get_project_data();


             }


             });


             } else {
             SweetAlert.swal("Your file is safe!");
             }
             });
        };


       $scope.rename= function (task) {
          // console.log(task);
           $scope.rename_task = task;
         //  console.log($scope.rename_task.task_name);

           $('#rename_task').openModal();

       };

        $scope.update_task = function (rename_task) {
            console.log('renm',rename_task);

            $http.post('api/update-task',rename_task).then(function(result){

                if(result.data.status === 200) {

                  //  $scope.data.project.project_name = result.data.project_name;

                    $scope.get_project_data();

                    $scope.rename_task = {};


                }

            });

        };


        $scope.delete = function (task) {
            $scope.delete_task = task;

        //    console.log('delete',$scope.delete_task);

            var index = utility.findObjectIndex($scope.data.project.tasks, 'task_id', $scope.delete_task.task_id);

            console.log('index',index);


            SweetAlert.swal({
                    title: "Are you sure want to delete " + $scope.delete_task.task_name + "?", //Bold text
                    type: "warning", //type -- adds appropiriate icon
                    showCancelButton: true, // displays cancel btton
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: true, //do not close popup after click on confirm, usefull when you want to display a subsequent popup
                    closeOnCancel: true
                },
                function (isConfirm) { //Function that triggers on user action.
                    if (isConfirm) {

                     //   $scope.delete_pro = {};
                     //   $scope.delete_pro.id = $scope.data.project.id;

                        $http.post('api/delete-task',$scope.delete_task).then(function(result){

                            if(result.data.status === 200) {

                                $scope.get_project_data();


                                // newVal is defined
                                $scope.data.project.tasks.splice(index, 1);

                  //              SweetAlert.swal("Deleted!");

                                //   $scope.get_project_data();

                            }

                        });


                    } else {
                        SweetAlert.swal("Your file is safe!");
                    }
                });

        };



    }
]);