angular.module('timeTrackerApp').controller('ReviewEntryController',[
    '$scope','$http','$cookies','$location',function ($scope, $http, $cookies, $location) {

        $scope.review_entry = {};

        $scope.review_entry.projectId = "";


        $scope.get_projects = function () {
            var user_id = $cookies.getObject('user');
            $http.get('api/projects/'+user_id.user.id).then(function (result) {

                console.log(result.data);
                $scope.projects = result.data;

            });
        };

        $scope.get_projects();


        $scope.get_task_log = function(project)  {
            console.log('pro info',project);

            $http.get('api/get-task-logs/'+project).then(function (result) {

                // console.log(result.data);
                $scope.task_logs = result.data;
            });

        }






    }
]);
