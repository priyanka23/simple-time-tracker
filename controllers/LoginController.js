angular.module('timeTrackerApp').controller('LoginController',['$scope', '$rootScope', '$cookies' ,'$http', '$location',function($scope, $rootScope, $cookies, $http, $location) {

    $scope.user = {};
    $scope.userInfo = {};
    $scope.loginInfo = {};
    $scope.user.userProfile = {};
    $scope.errors = {};
    $scope.errorDiv = false;


    $scope.init = function () {

        $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
            console.log(userDetails);

            if(userDetails.email  != 'undefined'){
                $http.post('api/social-login', userDetails ).then(function (result) {

                    if (result.data.status === 200) {
                        console.log("user detail",userDetails);
                        $cookies.putObject('user_stt', result.data, {'path': '/'});
                        $location.path('/project');
                    }
                    else
                    {
                        $scope.status = result.data;
                        console.log($scope.status.errors);

                    }
                });
            }else{
                console.log("message email required");
          }
        });

        $rootScope.$on('event:social-sign-in-failed', function(event, userDetails){
            alert('some error occured please retry')
        });

    };



    $scope.register = function () {

        $scope.user =  $scope.userInfo;
        // $scope.user.userProfile.fName =  $scope.fNa
        console.log('user',$scope.user);

        $http.post('api/register-user', $scope.user).then(function (result) {

            if(result.data.status === 200)
            {
                console.log(result);
                $rootScope.registerMessage = 'You are registered successfully. \n Please verify your email to continue login!';

            }
            else
            {
                console.log(result.data.error);
                $scope.errorDiv = true;
                $scope.errors = result.data.error;
            }
            $scope.registerForm.$setPristine();
            $scope.registerForm.$setUntouched();

            $scope.userInfo = {};
        });

    };


    $scope.login = function() {
        $scope.user = $scope.loginInfo;

        $http.post('api/Login',$scope.user).then(function (result) {


            if(result.data.status === 200) {

                console.log('result.data ',result.data);

                $cookies.putObject('user_stt', result.data);

                $rootScope.authenticated = true;
                $location.path('/project');
            }
            else {

            }
        });

    };


    $scope.verifyEmailAddress = function (email) {
        //console.log($scope.forgotPass.email);
        if($scope.forgetPasswordForm.$valid) {

            $http.post('api/password-reset', email ).then(function (result) {

                console.log(result.data);

                if(result.data.status == 200) {

                    console.log("success");

                    alert('You mail has been sent successfully!');

                    $rootScope.registerMessage = 'Please check  your email to reset password!';

                    $location.path('/login');

                    //   $('#modal_forgot_password').modal('hide');

                    $scope.forgotPass = {};

                    $scope.forgetPasswordForm.$setUntouched();

                    $('#modal_forgot_password').closeModal();

                }else if(result.data.status == 404) {
                    $rootScope.failedSendMessage = "This email address is not registered with us.";
                }

            });
        }

    };


    $scope.init();
}]);
