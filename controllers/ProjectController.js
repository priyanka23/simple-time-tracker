angular.module('timeTrackerApp').controller ('ProjectController',[
    '$scope','$http','$location','$route','$cookies', '$timeout', 'utility',
    function ($scope, $http, $location, $route, $cookies, $timeout, utility) {

        //$scope.colors = ['#5700F1','#FF085C','#1597FF','#00AB09','#FB8B00','#4E342E','#FF3333','#0025F4'];

        $scope.customTaskStyle = '';


        $scope.project={};

        $scope.projectDisplayLimit = 10;

          /*   $scope.taskBackgroundColor = {
            'background-color': '#FF3333 !important'
        };*/

        
       // $scope.project_client = 'Select Client';


        $scope.project.clientId = "";


        $scope.customTaskButtonStyle = 'task_background_7';


    //   $scope.taskBackgroundColor.css('background-color', '#FF3333 !important');


        $scope.colors = [
            {
                color_code: '#5700F1',
                color_class: 'c1',
                task_code:'task_background_1'
            },
            {
                color_code: '#FF085C',
                color_class: 'c2',
                task_code:'task_background_2'
            },
            {
                color_code: '#1597FF',
                color_class: 'c3',
                task_code:'task_background_3'
            },
            {
                color_code: '#00AB09',
                color_class: 'c4',
                task_code:'task_background_4'
            },
            {
                color_code: '#FB8B00',
                color_class: 'c5',
                task_code:'task_background_5'
            },
            {
                color_code: '#4E342E',
                color_class: 'c6',
                task_code:'task_background_6'
            },
            {
                color_code: '#FF3333',
                color_class: 'c7',
                task_code:'task_background_7'
            },
            {
                color_code:'#0025F4',
                color_class: 'c8',
                task_code:'task_background_8'
            }];

        $scope.projectColorCode = '#FF3333';

        $scope.projectIconName = 'new_0001_businessman.png';

        $scope.tasks = [];

        $scope.addTask =function(){
                $scope.tasks.push($scope.project.newTask);
                $scope.project.newTask = '';
                console.log('task',$scope.tasks);
        };

        $scope.applyClass = function (color) {
            // console.log (utility.findObjectIndex($scope.colors, 'color_code', color), color);
            var index = utility.findObjectIndex($scope.colors, 'color_code', color);
            return $scope.colors[index].color_class;
            
        };

        $scope.showProject = function () {

            var user_id = $cookies.getObject('user_stt');

            var u_id = user_id.user.id;

            $http.get('api/projects/'+u_id).then(function (result) {

                $scope.projects = result.data;

            });
        };

        $scope.showProject();


        $scope.cancel = function () {

        };

        $scope.style = function(color_value,color_class,task_code) {
            console.log('color',color_value);

            $scope.myColor = {
                "background-color" : color_value
            };

            $scope.projectColor = $scope.myColor;

            $scope.projectColorCode = color_value;

       //     $scope.customTaskStyle = 'some-css-class';

            $scope.customTaskStyle = color_class;

            $scope.customTaskButtonStyle = task_code;
            
            

            console.log(color_class);

         /*   angular.forEach($scope.colors,function(color,index){
                if(color == value) {
                   // $scope.customTaskStyle = color.color_class;
                    console.log('color chosen',color.color_class);
                }

            });*/


            $http.get('api/get-icons').then(function (result) {
                console.log('res',result.data);
                $scope.icons = result.data;
            });


            $('#demoModal').closeModal();
            $('#iconModal').openModal();

        };


        $scope.setIcon = function (icon) {
           // console.log('icon',icon);

            $scope.projectIconName = icon;

           var projIcon = 'assets/images/icons/'+icon;

         //   console.log('icon',projIcon);

            $scope.myIcon = {
              /*  "background-color" : value*/
                "background-image":"url("+projIcon+")"
             };



            console.log('my icon',$scope.myIcon);




            $('#iconModal').closeModal();

         //   $scope.customTaskStyle =

        };


        $scope.mergeStyleObj = function(objectList) {
            var obj = {};
            objectList.forEach(function(x) {
                angular.extend(obj,x);
            });
            return obj;
        };

   


        $scope.editThis = function (pro) {
          console.log('pro',pro);
            $http.get('api/get-todos/'+pro.id).then(function (task_data) {
                // $scope.projects.key.tasks = task_data.data;
                // console.log( $scope.projects.key.tasks);
                if(task_data.data) {
                    console.log(task_data.data);
                    $scope.task_pro = task_data.data;
                 }

            });
        };


        $scope.addSingleTask = function (task_name, project_id) {
            console.log('task',task_name);
            console.log('project_id ',project_id);
            
          //  $scope.task = [];

            $scope.task =  {
                task_name: task_name,
                project_id: project_id
            };

            console.log($scope.task);


            $http.post('api/add-todo',$scope.task).then(function (result) {

                console.log(result);
                if(result.data.status === 200) {
                    $scope.message = "Task added successfully";
                    $scope.showProject();
                 //   $scope.projectAddForm.$setUntouched();
                }

            });

        };


        $scope.add_project =function () {

            $scope.project.projectColorCode = $scope.projectColorCode;
            $scope.project.projectIconName = $scope.projectIconName;

            $scope.project.tasks = $scope.tasks;

            console.log('project data',$scope.project);

            $scope.project.userId = $cookies.getObject('user_stt').user.id;

            $http.post('api/add-project',$scope.project).then(function (result) {

                console.log(result);
                if(result.data.status === 200) {
                    $scope.message = "Project added successfully";
                    $scope.showProject();

                    $scope.resetDropDown();

                    $scope.tasks = [];


                }

            });


            $scope.project = {};
            $scope.projectAddForm.$setPristine();
            $scope.projectAddForm.$setUntouched();

        };





        // $scope.get_client = function () {
        //     // $http.get('api/client.php/get-client').then(function (result) {
        //     //
        //     //     console.log('res',result.data);
        //     //     $scope.clients = result.data;
        //     // });
        // };
        //
        //
        // $scope.get_client();




        $scope.resetDropDown = function() {
            if(angular.isDefined($scope.project.clientName)){
                delete $scope.project.clientName;
            }
        };
        
        $scope.more_projects = function () {
            $scope.projectDisplayLimit += 10;
        };




   /*      $scope.counter = 0;
         $scope.stopped = true;

         $scope.timer_icon ='play_arrow';
        $scope.onTimeout = function(){
            $scope.counter++;
            mytimeout = $timeout($scope.onTimeout,1000);
         };
         $scope.takeAction = function(){
             if(!$scope.stopped){
                 $timeout.cancel(mytimeout);
                $scope.timer_icon ='play_arrow';
            }
             else
             {
                mytimeout = $timeout($scope.onTimeout,1000);
                $scope.timer_icon ='stop';
            }
             $scope.stopped=!$scope.stopped;
         };
*/


        $scope.task_timer = {};

        var mytimeout;


        $scope.onTimeout = function(){
            $scope.task_timer.task_time_log++;

            console.log('taskk ....',$scope.task_timer.stopped);

            mytimeout = $timeout($scope.onTimeout,1000);

        };





        $scope.startTimer = function (task_log, project) {
            //    console.log(task);

            //  task_log.task_time_log = 0;
            console.log(task_log);


   //         console.log('start time',   $scope.task_timer.start_time);


            $scope.task_timer = task_log;

        //    $scope.task_timer.stopped = false;

         //   console.log('pro log',project_info);

           // $scope.project = project_info;

            project.task_info =  $scope.task_timer;



            if(angular.isUndefined($scope.task_timer.start_time) || $scope.task_timer.task_time_log == 0 ) {
                $scope.task_timer.start_time = $scope.timeNow();

                $scope.task_timer.date = $scope.formatDate();

                $scope.task_timer.task = $scope.task_timer.task_id;

                console.log('task id', $scope.task_timer.task);
            }
            else {

            }

            console.log('start time',   $scope.task_timer.start_time);


            $scope.task_timer.stopped = !$scope.task_timer.stopped;

            $scope.task_timer.timer_icon ='play_arrow';



            if ($scope.task_timer.stopped) {

                project.task_info = "";

                $scope.task_timer.end_time =  $scope.timeNow();

                console.log('stop time', $scope.task_timer.end_time);
                $timeout.cancel(mytimeout);
                $scope.task_timer.timer_icon = 'play_arrow';
                if( $scope.task_timer.task_time_log > 0) {
                    console.log('time log after stop',$scope.task_timer.task_time_log);

                    $('#taskTimerModal').openModal();
                    
                }
            }
            else {
                mytimeout = $timeout($scope.onTimeout, 1000);
                $scope.task_timer.timer_icon = 'stop';
            }




        };






 /*       $scope.startTimer = function (task_log) {

          //  task_log.task_time_log = 0;
            console.log(task_log);



            $scope.task_timer = task_log;

            console.log('before',$scope.task_timer.stopped);

            $scope.task_timer.stopped = true;

            console.log('dfdfdffd',$scope.task_timer.stopped);


            if(!$scope.task_timer.stopped){
                $timeout.cancel(mytimeout);
                $scope.task_timer.timer_icon ='play_arrow';
            }
            else
            {
                mytimeout = $timeout($scope.onTimeout,1000);
                $scope.task_timer.timer_icon ='stop';
            }
            $scope.task_timer.stopped=!$scope.task_timer.stopped;


        };*/


        $scope.add_time_log = function(task_memo) {
            console.log(task_memo);

            console.log($scope.task_timer.task_time_log);

            $('#taskTimerModal').closeModal();


            if($scope.task_timer.task_time_log < 60) {
                alert('Task time less than 1 min. Cannot save log');

            }
            else {

          //      console.log($scope.task_timer);

                $scope.task_timer.time_diff_minutes = Math.floor($scope.task_timer.task_time_log / 60);

                console.log($scope.task_timer);


               $http.post('api/task-log-entry',$scope.task_timer).then(function (result) {

                    console.log(result.data);
                    if(result.data.status === 200) {

                       /* $scope.message = "Task added successfully";
                        $scope.showProject();*/
                        //   $scope.projectAddForm.$setUntouched();
                    }


                });
            }

            $scope.task_timer.task_time_log = 0;
            $scope.task_timer.memo = '';
            $scope.task_timer = {};

            console.log('save',$scope.task_timer);

        };




        $scope.timeNow  = function () {
            var d = new Date(),
                h = (d.getHours()<10?'0':'') + d.getHours(),
                m = (d.getMinutes()<10?'0':'') + d.getMinutes(),
                s = (d.getSeconds()<10?'0':'') + d.getSeconds();
            return  h + ':' + m + ':' + s;
        };



        $scope.formatDate = function() {
            var d = new Date(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day,month,  year ].join('/');
        };



        $scope.close_timer_modal = function () {
            if(angular.isUndefined($scope.task_timer)) {

            }   else {
                $scope.task_timer.task_time_log = 0;
                $scope.task_timer = {};
            }
        }


    }

]);