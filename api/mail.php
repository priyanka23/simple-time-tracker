<?php
error_reporting(E_ALL);
?>

<html>
<head>
    <title></title>

    <!-- Materialize CSS -->
    <link rel="stylesheet" href="../assets/materialize/css/materialize.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <link href="../assets/material_design_icons/materialdesignicons.css" media="all" rel="stylesheet" type="text/css" />



    <script type="text/javascript" src="../assets/js/jquery-3.0.0.min.js"></script>
    <script type="text/javascript" src="../assets/materialize/js/materialize.min.js"></script>
    <!--    <script type="text/javascript" src="assets/lib/angular-materialize.min.js"></script>-->

    <link rel="stylesheet" href="../assets/css/app.css">

</head>
<body>

<div class="row">
    <div class="container">
        <div class="col s6">
            <form class="" method="post">

                <h5>New Password</h5>
                <div class="input-field col s12">
                    <input id="new_password" type="text" class="validate text-field input_time_tacker" name="new_password" required>
                    <label for="icon_prefix">New Password</label>

                    <input type="hidden" id="reset_token" value="<?= $_GET['password_reset_token']; ?>"/>

                </div>

                <div class="input-field col s12">
                <button type="submit" class="btn waves-effect waves-light"  name="action" id="save_new">Save
                    <!--  <i class="material-icons right">send</i>-->
                </button>
               </div>


                <div class="msg">

                </div>

            </form>
        </div>

    </div>
</div>


</body>
<script>
    $('#save_new').click(function (e) {

     //   alert(1);

        e.preventDefault();

        var new_password =  $('#new_password').val();
        var token = $('#reset_token').val();

        $.ajax({
            url: "save_password.php",
            type: "POST",
        /*    contentType: "application/json; charset=utf-8",*/
            dataType: "json",
            data: {new_password:new_password,token:token},
            success: function (result) {
                console.log('res',result);
                if (result.status = 200) {
                    //    alert(1);
                    $('#new_password').val('');
                    $('.msg').html(result.message);
                    $('.msg').css('color','green');
                }
            }

        });

    });
</script>

</html>

