<?php

require 'Slim/Slim.php';
/*error_reporting(E_ALL);*/

$app = new Slim();

$app->post('/create-client', 'createClient');
$app->get('/get-clients/', 'getClients');
/*$app->get('/verify-email/:email/:verificationCode','verifyEmail');
$app->post('/password-reset','resetPassword');
$app->post('/Login', 'checkLoginDetails');
$app->get('/reset-pass/:password_reset_token','passwordReset');

$app->get('/get-icons','getIcons');
$app->post('/add-task','addSingleTask');

$app->post('/add-project','addProject');
$app->get('/projects/:user_id', 'getProjects');  */
$app->get('/get-client','getClient');


$app->run();

// Get Database Connection
function DB_Connection() {
    $dbhost = "127.0.0.1";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "simple_time_tracker";

    /*	$dbhost = "localhost";
        $dbuser = "mithilas";
        $dbpass = "Tacktile@786";
        $dbname = "mithilas_simple_time_tracker";*/


    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

/* function to create client */
function createClient() {
    $request = Slim::getInstance()->request();

    $client = json_decode($request->getBody());

  //  var_dump($client);

    $date = new DateTime();
    $date=  $date->format('Y-m-d H:i:s');

    $epoch = time();

    $organization_id = 1;

    if(isset($client->id) && !empty($client->id)) {
        updateClient($client, $epoch, $date);
    }
    else {
        insertClient($client, $epoch, $date);
    }
}
/* end of function to create client  */


/* function to get all clients */
function getClients() {
 //   $sql = "SELECT * FROM client";
    $sql = "  SELECT
	client.id,
        client.name,
        client.contact_person_name,
        client.email,
        client.number,
        client.address_line1,
        client.business_id,
        client.business_detail,
        COUNT(project.id) AS `project_count`
    FROM
        client
    LEFT JOIN
        project
    ON
        client.id=project.client_id
     GROUP BY client.id,
           project.client_id";
    try {
        $db = DB_Connection();
        $stmt = $db->query($sql);
        $clients = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        /*	var_dump($projects);*/
        //	$projects->status = 200;
        echo json_encode($clients);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

}
/* end of function to get clients */


/* function to insert client */
 function insertClient($client, $date, $epoch)  {
     $organization_id  = 1;

     $sql = "INSERT INTO client(organization_id, name, contact_person_name, email, number, address_line1, business_detail, business_id, created_on, modified_on, epoch) VALUES (:organization_id, :name, :contact_person_name, :email, :number, :address_line1, :business_detail, :business_id, :created_on, :modified_on, :epoch)";
     try {
         $db = DB_Connection();
         $stmt = $db->prepare($sql);

         $stmt->bindParam("organization_id", $organization_id);
         $stmt->bindParam("name", $client->name);
         $stmt->bindParam("contact_person_name", $client->contact_person_name);
         $stmt->bindParam("email", $client->email);
         $stmt->bindParam("number", $client->number);
         $stmt->bindParam("address_line1", $client->address_line1);
         $stmt->bindParam("business_detail",$client->business_detail);
         $stmt->bindParam("business_id",$client->business_id);
         $stmt->bindParam("created_on",$date);
         $stmt->bindParam("modified_on",$date);
         $stmt->bindParam("epoch",$epoch);
         $stmt->execute();
         $client->id = $db->lastInsertId();

         $client->status = 200;
         $client->message = "Client Added Successfully";
         $db = null;
         echo json_encode($client);
     }
     catch(PDOException $e) {
 /*       echo '{"error":{"text":'. $e->getMessage() .'}}';*/
         $client->status = 500;
         $client->message = "Server Internal Error";
         echo json_encode($client);
     }
 }


/* function to update client */
function updateClient($client, $date, $epoch) {

    $sql = "UPDATE client SET organization_id =:organization_id, name =:name, contact_person_name =:contact_person_name, email =:email, number =:number, address_line1 =:address_line1, business_detail =:business_detail, business_id =:business_id, modified_on =:modified_on WHERE id =:id ";
    try {
        $db = DB_Connection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("organization_id", $client->organization_id);
        $stmt->bindParam("name", $client->name);
        $stmt->bindParam("contact_person_name", $client->contact_person_name);
        $stmt->bindParam("email", $client->email);
        $stmt->bindParam("number", $client->number);
        $stmt->bindParam("address_line1", $client->address_line1);
        $stmt->bindParam("business_detail",$client->business_detail);
        $stmt->bindParam("business_id",$client->business_id);
       // $stmt->bindParam("created_on",$date);
        $stmt->bindParam("modified_on",$date);
       // $stmt->bindParam("epoch",$epoch);
        $stmt->bindParam("id", $client->id);
        $stmt->execute();
        $db = null;

        $client->status = 200;
        $client->message = "Client Updated Successfully";
        $db = null;
        echo json_encode($client);

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
      ///  $client->status = 500;
     //   $client->message = "Server Internal Error";
      //  echo json_encode($client);
    }

}
/* end of function to update client */



/* function to get client for projects */
function getClient() {
//    $sql = "SELECT id, name FROM client";
//    try {
//        $db = DB_Connection();
//        $stmt = $db->query($sql);
//        $clients = $stmt->fetchAll(PDO::FETCH_OBJ);
//        $db = null;
//        echo json_encode($clients);
//    } catch(PDOException $e) {
//        echo '{"error":{"text":'. $e->getMessage() .'}}';
//    }


    $sql = "SELECT id, name FROM client";
    try {
        $db = DB_Connection();
        $stmt = $db->query($sql);
        $clients = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        /*	var_dump($projects);*/

        //	$projects->status = 200;
        echo json_encode($clients);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }


}
/* end of get client function */

