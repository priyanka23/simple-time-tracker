<?php

require 'Slim/Slim.php';
/*error_reporting(E_ALL);*/

$app = new Slim();

$app->post('/create-user', 'createUser');
$app->get('/verify-email/:email/:verificationCode','verifyEmail');
$app->post('/password-reset','resetPassword');
$app->post('/Login', 'checkLoginDetails');
$app->get('/reset-pass/:password_reset_token','passwordReset');







$app->get('/get-icons','getIcons');
$app->post('/add-task','addSingleTask');

$app->post('/add-project','addProject');
$app->get('/projects/:user_id', 'getProjects');
$app->get('/get-tasks/:id','getTasks');


$app->get('/get-project-tasks/:id','getProjectTasks');




$app->post('/task-log-entry','taskLogEntry');


$app->post('/update-project','updateProject');

$app->post('/delete-project','deleteProject');

$app->post('/update-task','updateTask');

$app->post('/delete-task','deleteTask');

$app->get('/get-task-logs/:id','getTaskLogs');





/* $app->delete('/PostDelete/:id', 'deletePost');
$app->get('/Posts/:id', 'getPost');
$app->put('/Post/:id', 'updatePost');
$app->post('/New_User','addUser');  */
$app->get('/validate-token','isValidateToken');


$app->run();

// Get Database Connection
function DB_Connection() {
	$dbhost = "127.0.0.1";
	$dbuser = "root";
	$dbpass = "";
	$dbname = "simple_time_tracker";

/*	$dbhost = "localhost";
	$dbuser = "mithilas";
	$dbpass = "Tacktile@786";
	$dbname = "mithilas_simple_time_tracker";*/


	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}



// Check login details
function checkLoginDetails() {
	$request = Slim::getInstance()->request();
	$user = json_decode($request->getBody());

	/*var_dump($user);*/
  //  print_r($user);
	$user_info =  (object)[];

	$sql = "select id, first_name, email, organization FROM user WHERE email ='".$user->username."' AND password = '".$user->password."'";

	// $sql = "select id,username,access_token FROM user WHERE username ='$user->Username' AND password = '$user->Password'";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$list = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
	//	$access_token = generateAccessToken($list->id);
	//	$list->access_token = $access_token;
		if(!empty($list)){
			//echo "success";

			$access_token = generateAccessToken($list->id);
	/*
			$user->name = $list->firstname;
			$user->id = $list->id;
			$user->email = $list->email;
			$user->organization = $list->organization;*/
			$user_info->status = 200;
			$user_info->messsage = 'Login Successful';
			$user_info->access_token = $access_token;
			$user_info->user = $list;



			//	print_r($list);
			echo json_encode($user_info);
		}
		else {
			echo '{"status":401,"msg":"invalid credentials"}';
		}

	//	echo json_encode($list);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}


// Add new User to the Database
function createUser() {
	$request = Slim::getInstance()->request();

	 $user = json_decode($request->getBody());


	$date = new DateTime();
	$timestamp =  $date->getTimestamp();

	//Generate a random string.
	$token = openssl_random_pseudo_bytes(16);

	//Convert the binary data into hexadecimal representation.
	$token = bin2hex($token).'-'.$timestamp;

	$email_verification_code = $token;


      $epoch = time();


	$sql = "INSERT INTO user(first_name, last_name, email, password, organization, email_verification_code, epoch_timestamp) VALUES (:first_name, :last_name, :email, :password, :organization, :email_verification_code, :epoch_timestamp)";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("first_name", $user->firstName);
		$stmt->bindParam("last_name", $user->lastName);
		$stmt->bindParam("email", $user->email);
		$stmt->bindParam("password", $user->password);
		$stmt->bindParam("organization", $user->organization);
		$stmt->bindParam("email_verification_code",$email_verification_code);
		$stmt->bindParam("epoch_timestamp",$epoch);
		$stmt->execute();
		$user->id = $db->lastInsertId();

		if($user->id) {
			sendMail($user->firstName, $user->email, $email_verification_code);
		}

		$user->status = 200;
		$user->message = "Registered Successfully";
		$db = null;
		echo json_encode($user);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
/* end of create user function */


/* start of send mail function */
function sendMail($name, $email, $email_verification_code) {

//	$message = "Your Activation Code is ".$code."";
	$to=$email;
	$subject="Simple Time Tracker Email Verification";
	$from = 'donotreply@gmail.com';

	//echo $name. '  '.$email;


	$url =  'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']).'/verify-email/'.$email . '/' . $email_verification_code;

	$body ='';

	$body='Hi ' .$name.', <br/><br/> Please click on the following link to verify your email <br/><br/> <a href="'.$url.'">'.$url.'</a> <br/><br/> to activate  your account.';

	echo $body;

	$headers = "From:".$from;
	$headers .= "Return-Path: ".$from."\r\n";
	$headers .= 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

	mail($to,$subject,$body,$headers);

}
/* end of send mail function */


/* function to verify email */
function verifyEmail($email, $verificationCode){

	$timestamp2 = time();

	$sql = "select * FROM user WHERE email= '$email' AND email_verification_code = '$verificationCode'";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$list = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		if(!empty($list)){

			$timestamp1 = $list->epoch_timestamp;

			$diff = abs($timestamp2 - $timestamp1) / 3600;
            $time_diff =  floor($diff);

		//	echo "time 1 = ".$timestamp1;
		//	echo "<br/>time 2= ".$timestamp2;

			//echo "<br/>diff = " .$diff;


			if($time_diff < 24) {

				$new_code = '';
				$email_verification= 1;

				$id= $list->id;

			//	echo "id =".$id;

				$sql = "UPDATE user SET email_verification_code =:email_verification_code ,email_verification =:email_verification WHERE id =:id ";
				try {
					$db = DB_Connection();
					$stmt = $db->prepare($sql);
					$stmt->bindParam("email_verification_code", $new_code);
					$stmt->bindParam("email_verification", $email_verification);
					$stmt->bindParam("id", $id);
					$stmt->execute();
					$db = null;

					echo "Email verified successfully!!!";


				} catch(PDOException $e) {
					echo '{"error":{"text":'. $e->getMessage() .'}}';
				}


			}





			//echo "success";
		//	$list->status = 200;
		//	$list->messsage = 'Valid access token';
			//	print_r($list);
		//	echo json_encode($list);
		}
		else {
			echo '{"status":404,"msg":"invalid auth token"}';
		}
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}
/* end of email verify function */


/* function to reset password */
function passwordReset($password_reset_token) {
	//echo "pass ".$password_reset_token;



}

/* end of reset password function */





/* function to send password reset mail */
function sendPasswordResetMail($email,$user_info,$reset_password_token) {
	$to=$email;
	$subject="Simple Time Tracker Password Reset";
	$from = 'donotreply@gmail.com';

	$url =  'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']).'/mail.php?password_reset_token='.$reset_password_token;


	//echo $url;
	$body ='';

	$body='Hi ' .$user_info->first_name.', <br/><br/> Please click on the following link to reset your password <br/><br/> <a href="'.$url.'">'.$url.'</a> <br/><br/> to activate  your account.';

//	echo $body;

	$headers = "From:".$from;
	$headers .= "Return-Path: ".$from."\r\n";
	$headers .= 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

	mail($to,$subject,$body,$headers);
}
/* end of function to send password reset mail */





function resetPassword() {
	$request = Slim::getInstance()->request();
	$user = json_decode($request->getBody());

	$email=  $user->email;

	$sql = "SELECT * FROM user WHERE email = '$email'";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$user_info = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
	/*	var_dump($user_info);*/

		if(!empty($user_info)) {
			$date = new DateTime();
			$timestamp =  $date->getTimestamp();

			//Generate a random string.
			$reset_password_token = openssl_random_pseudo_bytes(16);
			//Convert the binary data into hexadecimal representation.
			$reset_password_token = bin2hex($reset_password_token).'-'.$timestamp;

			$sql = "UPDATE user SET password_reset_token =:password_reset_token WHERE id=:id";
			try {
				$db = DB_Connection();
				$stmt = $db->prepare($sql);
				$stmt->bindParam("password_reset_token", $reset_password_token);
				$stmt->bindParam("id", $user_info->id);
				$stmt->execute();
				$db = null;

				sendPasswordResetMail($email, $user_info, $reset_password_token);

				$user->status=200;
				$user->message= 'Please check your email to get password reset link';
				echo json_encode($user);


				//	return $token;
			} catch(PDOException $e) {
				echo '{"error":{"text":'. $e->getMessage() .'}}';
			}

		}
		else {
			$user->status=401;
			$user->message= 'This email is not registered with us';
			echo json_encode($user);
		}

		//echo json_encode($user_info);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}




//	var_dump($user);

}



/*  function to get icon list from folder */
function getIcons(){
	$path    = '../assets/images/icons';
	$files = scandir($path);

	//Following code will remove . and .. from the returned array from scandir:
    $files = array_diff(scandir($path), array('.', '..'));

	//var_dump($files);

	echo json_encode($files);
}
/* end of function to get icon list from folder */






function generateAccessToken($user_id){

	$date = new DateTime();
	$timestamp =  $date->getTimestamp();

	//Generate a random string.
	$token = openssl_random_pseudo_bytes(16);

	//Convert the binary data into hexadecimal representation.
 	$token = bin2hex($token).'-'.$timestamp;

	$sql = "UPDATE user SET access_token =:access_token WHERE id=:id";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("access_token", $token);
		$stmt->bindParam("id", $user_id);
		$stmt->execute();
		$db = null;
		return $token;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function isValidateToken() {
	$request =Slim::getInstance()->request();
	$id = $request->get('id');
	$token = $request->get('token');

	$user_info =  (object)[];

	$sql = "select id, first_name, email, organization FROM user WHERE id = ".$id." AND access_token = '$token'";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$list = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		//$list->access_token = $access_token;
		if(!empty($list)){
			//echo "success";
			$user_info->status = 200;
			$user_info->messsage = 'Valid access token';
			$user_info->user = $list;

		//	print_r($list);
			echo json_encode($user_info);
		}
		else {
			echo '{"status":401,"msg":"invalid auth token"}';
		}
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}


/* function to add new project */
function addProject(){
	$request = Slim::getInstance()->request();
	$project = json_decode($request->getBody());

	$date =  $date = date('Y-m-d H:i:s');

	$sql = "INSERT INTO project (project_name, client_id, project_price, project_icon, project_color, user_id) VALUES (:project_name, :client_id, :project_price, :project_icon, :project_color, :user_id)";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("project_name", $project->projectName);
		$stmt->bindParam("client_id", $project->clientId);
		$stmt->bindParam("project_price", $project->projectPrice);
		$stmt->bindParam("project_icon", $project->projectIconName);
		$stmt->bindParam("project_color", $project->projectColorCode);
		$stmt->bindParam("user_id", $project->userId);
		$stmt->execute();
		
		$project->id = $db->lastInsertId();


		if(!empty($project->tasks) && count($project->tasks) > 0){
			$res =   addTask($project->tasks,$project->id);
		}else{
			//echo "empty ";
		}

		$project->status = 200;
		$project->messsage = 'Project Added Successfully';


		//var_dump($res);

		$db = null;
		echo json_encode($project);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
/* end of the function to add new project */



/* function to add tasks for the project */
function addTask($tasks,$project_id) {
	/*var_dump($tasks);*/

	$sql = "INSERT INTO task SET project_id =:project_id, task_name=:task_name";
	try {

		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		foreach($tasks as $d) {
			$stmt->execute(array(':project_id' => $project_id, ':task_name' => $d));
		}
		$db = null;

		$response = Slim::getInstance()->response();
		$response->status(200);

		return $response;
	//	$response =array();
	//	$response->status = 200;
	//	$response->msg ="Added successfully";
	//	return $response;
		//echo json_encode($project);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
	return;
}
/* end of the function to add tasks */


/* function to get list of projects */
function getProjects($user_id) {
	$sql = "SELECT * FROM project WHERE  user_id = ".$user_id;
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$projects = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
	/*	var_dump($projects);*/
		if(!empty($projects)) {
			foreach($projects as $pro) {
				$task = getTasks($pro->id);
				$pro->tasks = $task;
			}
		}
	//	$projects->status = 200;
		echo json_encode($projects);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
/*end of the function to get list of projects*/

/* function to get tasks of a project */
function getTasks($id){
	//echo $id;
	$sql = "select id as task_id,task_name FROM task WHERE project_id=".$id." ORDER BY id";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
	//	$list = $stmt->fetch(PDO::FETCH_OBJ);
		$list = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
			//return($list);
	//	$list->status = 200;
		return $list;
	//	echo json_encode($list);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

/* end of function to get tasks of a project*/


/* function to add Single Task  */
function addSingleTask() {
	$request = Slim::getInstance()->request();
	$task = json_decode($request->getBody());
   // var_dump($task);
//	echo $task->project_id;
//	echo $task->task_name;

	$sql = "INSERT INTO task SET project_id =:project_id, task_name=:task_name";
	try {

		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("project_id", $task->project_id);
		$stmt->bindParam("task_name",  $task->task_name);
		$stmt->execute();
		$task->id = $db->lastInsertId();
		$task->status = 200;
		$task->message = "Task Added Successfully";
		echo json_encode($task);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
/* End of add Single Task function */



/* function for updating password in the database */
function setPassword($token,$new_password) {

	$token_reset ='';
	$sql = "UPDATE user SET password =:new_password ,password_reset_token =:password_reset_token WHERE password_reset_token =:token_reset ";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("new_password", $new_password);
		$stmt->bindParam("password_reset_token", $token);
		$stmt->bindParam("token_reset", $token_reset);
		$stmt->execute();
		$db = null;

		echo "Email verified successfully!!!";


	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}
/* end of function for updating password */


/* function to get tasks for particular project */
function getProjectTasks($project_id) {
	//echo 'pro id = '.$project_id;
	$sql = "SELECT * FROM task WHERE project_id = ".$project_id;
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$tasks = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		/*	var_dump($projects);*/
		//	$projects->status = 200;
		echo json_encode($tasks);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}
/* end of function to get project tasks */

/* function to add data from manual entry and project module*/
function taskLogEntry() {
	$request = Slim::getInstance()->request();
	$task_data = json_decode($request->getBody());

//	var_dump($task_data);

	$date = $task_data->date;
	$date = str_replace('/', '-', $date);
	$date = date('Y-m-d', strtotime($date));

	$sql = "INSERT INTO project_task_time_log SET task_id =:task_id, task_date =:task_date, start_time =:start_time, end_time =:end_time, memo =:memo, time_difference =:time_difference";
	try {

		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("task_id", $task_data->task);
		$stmt->bindParam("task_date",  $date);
		$stmt->bindParam("start_time",  $task_data->start_time);
		$stmt->bindParam("end_time",  $task_data->end_time);
		$stmt->bindParam("memo",  $task_data->memo);
		$stmt->bindParam("time_difference", $task_data->time_diff_minutes);

		$stmt->execute();
		$task_data->id = $db->lastInsertId();
		$task_data->status = 200;
		$task_data->message = "Task Added Successfully";
		echo json_encode($task_data);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
/* end of function to add manual entry data and project module data*/




/* function to update project name */
function updateProject() {
	$request = Slim::getInstance()->request();
	$project_data = json_decode($request->getBody());

	$sql = "UPDATE project SET project_name =:project_name WHERE id=:id";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("project_name", $project_data->project_name);
		$stmt->bindParam("id", $project_data->project_id);
		$stmt->execute();
		$db = null;
		
		$project_data->status=200;
		$project_data->message= 'Project Updation Successful';
		echo json_encode($project_data);

		//	return $token;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}


}
/* end of function to update project name */


/* function to delete project */
function deleteProject() {
	$request = Slim::getInstance()->request();
	$project_data = json_decode($request->getBody());

//	var_dump($project_data);

	$sql = "DELETE FROM task WHERE project_id=:id";
	//var_dump($id);
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $project_data->id);
		$stmt->execute();


		$sql1 = "DELETE FROM project WHERE id=:id";
		//var_dump($id);
		$stmt1 = $db->prepare($sql1);
		$stmt1->bindParam("id", $project_data->id);
		$stmt1->execute();

		$project_data->status = 200;
		$project_data->message = 'Deleted Successfully';

		echo json_encode($project_data);

	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}
/* end of function to delete project */


/* function to update project task */
function updateTask() {
	$request = Slim::getInstance()->request();
	$task_data = json_decode($request->getBody());

	//var_dump($task_data);

	$sql = "UPDATE task SET task_name =:task_name WHERE id=:id";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("task_name", $task_data->task_name);
		$stmt->bindParam("id", $task_data->task_id);
		$stmt->execute();
		$db = null;

		$task_data->status=200;
		$task_data->message= 'Project Updation Successful';
		echo json_encode($task_data);

		//	return $token;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}


}
/* end of function to update project task */


/*  function to delete project task  */
function deleteTask() {
	$request = Slim::getInstance()->request();
	$task_data = json_decode($request->getBody());

//	var_dump($project_data);

	$sql = "DELETE FROM task WHERE project_id=:id";
	//var_dump($id);
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $task_data->id);
		$stmt->execute();

		$sql1 = "DELETE FROM task WHERE id=:id";
		//var_dump($id);
		$stmt1 = $db->prepare($sql1);
		$stmt1->bindParam("id", $task_data->task_id);
		$stmt1->execute();

		$task_data->status = 200;
		$task_data->message = 'Deleted Successfully';

		echo json_encode($task_data);

	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
/* end of function to delete task */



/* function to get data for task logs */
function getTaskLogs($project_id) {
	/*var_dump($project_id);*/

	// $sql = "select id as task_id,task_name FROM task WHERE project_id=".$id." ORDER BY id";
	$sql = "SELECT t.id, t.task_name, t.project_id, p.start_time, p.end_time, p.task_date, p.time_difference FROM project_task_time_log AS p INNER JOIN task AS t ON p.task_id = t.id WHERE t.project_id =".$project_id;
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		//	$list = $stmt->fetch(PDO::FETCH_OBJ);
		$list = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		//return($list);
		//	$list->status = 200;
		echo json_encode($list);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}

}
/* end of function to get task logs */


?>