<?php

require 'Slim/Slim.php';

$app = new Slim();

$app->get('/Posts', 'getPosts');
$app->post('/Login', 'checkLoginDetails');
$app->post('/New_Post','addPost');
$app->delete('/PostDelete/:id', 'deletePost');
$app->get('/Posts/:id', 'getPost');
$app->put('/Post/:id', 'updatePost');
$app->post('/New_User','addUser');
$app->get('/validate-token','isValidateToken');


$app->run();

// Get Database Connection
function DB_Connection() {	
	$dbhost = "127.0.0.1";
	$dbuser = "root";
	$dbpass = "";
	$dbname = "simple_time_tracker";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

//Get posts Details
function getPosts() {
	$sql = "select id,post_title,post_content,post_date,user_id FROM post";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);  
		$list = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($list);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


// Check login details
function checkLoginDetails() {
	$request = Slim::getInstance()->request();
	$user = json_decode($request->getBody());

	//var_dump($user);
  //  print_r($user);
	$sql = "select id,username,access_token FROM user WHERE username ='$user->Username' AND password = '$user->Password'";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$list = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		$access_token = generateAccessToken($list->id);
		$list->access_token = $access_token;
		echo json_encode($list);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}


// Add new Post to the Database
function addPost() {
	$request = Slim::getInstance()->request();
	$post = json_decode($request->getBody());

	$date =  $date = date('Y-m-d H:i:s');

	$sql = "INSERT INTO post (post_title, post_content, post_date, user_id) VALUES (:post_title, :post_content, :post_date, :user_id)";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("post_title", $post->title);
		$stmt->bindParam("post_content", $post->content);
		$stmt->bindParam("post_date", $date);
		$stmt->bindParam("user_id", $post->user_id);
		$stmt->execute();
		$post->id = $db->lastInsertId();
		$db = null;
		echo json_encode($post);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

/**
 * deletes the requested post
 *
 * @params $id
 * @return array of post after calling get posts after deleting the post with $id
 *
 */
function deletePost($id) {
		$sql = "DELETE FROM post WHERE id=:delete_id";
	//var_dump($id);
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("delete_id", $id);
		$stmt->execute();
		$db = null;
		getPosts();
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

// GET One Post Details
function getPost($id) {
	$sql = "select * FROM post WHERE id=".$id." ORDER BY id";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$list = $stmt->fetch(PDO::FETCH_OBJ);
		//$list = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
	//	var_dump($list);
		echo json_encode($list);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

//Update Post Details
function updatePost($id) {
	$request = Slim::getInstance()->request();
	$post = json_decode($request->getBody());

	// var_dump($post);
	$sql = "UPDATE post SET post_title=:title,post_content=:content WHERE id=:id";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("title", $post->post_title);
		$stmt->bindParam("content", $post->post_content);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
		echo '{"success":{"msg":"updated successfully"}}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}


// Add new User to the Database
function addUser() {
	$request = Slim::getInstance()->request();

	 $user = json_decode($request->getBody());



	$sql = "INSERT INTO user(username, password, email) VALUES (:username, :password, :email)";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("username", $user->username);
		$stmt->bindParam("password", $user->password);
		$stmt->bindParam("email", $user->email);
		$stmt->execute();
		$user->id = $db->lastInsertId();
		$db = null;
		echo json_encode($user);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function generateAccessToken($user_id){

	$date = new DateTime();
	$timestamp =  $date->getTimestamp();

	//Generate a random string.
	$token = openssl_random_pseudo_bytes(16);

	//Convert the binary data into hexadecimal representation.
 	$token = bin2hex($token).'-'.$timestamp;

	$sql = "UPDATE user SET access_token =:access_token WHERE id=:id";
	try {
		$db = DB_Connection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("access_token", $token);
		$stmt->bindParam("id", $user_id);
		$stmt->execute();
		$db = null;
		return $token;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function isValidateToken() {
	$request =Slim::getInstance()->request();
	$id = $request->get('id');
	$token = $request->get('token');
	$sql = "select * FROM user WHERE id = ".$id." AND access_token = '$token'";
	try {
		$db = DB_Connection();
		$stmt = $db->query($sql);
		$list = $stmt->fetch(PDO::FETCH_OBJ);
		$db = null;
		//$list->access_token = $access_token;
		if(!empty($list)){
			//echo "success";
			$list->status = 200;
			$list->messsage = 'Valid access token';
		//	print_r($list);
			echo json_encode($list);
		}
		else {
			echo '{"status":401,"msg":"invalid auth token"}';
		}
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}


?>