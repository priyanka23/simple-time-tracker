<?php
/**
 * Created by PhpStorm.
 * User: Priyanka
 * Date: 10/12/2016
 * Time: 3:54 PM
 */



    //var_dump($_POST);

    /* require the database connection file */
     require_once 'connection.php';


    # getConnection
    function getConnection(){
        global $conn;
        if(is_null($conn)){
            $conn = new Connection();
        }
        return $conn->getConnection();
    }


     $token = $_POST['token'];


      $token_reset = NULL;
      $sql = "UPDATE user SET password =:new_password ,password_reset_token =:token_reset WHERE password_reset_token =:password_reset_token";
      try {
            $db = getConnection();
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam("new_password", $_POST['new_password']);
            $stmt->bindParam("token_reset", $token_reset);
            $stmt->bindParam("password_reset_token",  $token);
            $stmt->execute();
            $dbh = null;
          //  echo "Email verified successfully!!!";

            $response = array();
            $response['status'] = 200;
            $response['message'] = 'Password reset successful';

            echo json_encode($response);


        } catch(PDOException $e) {
          //  echo '{"error":{"text":'. $e->getMessage() .'}}';
            $response = array();
            $response['status'] = 401;
            $response['message'] = $e->getMessage();
            echo json_encode($response);

      }


