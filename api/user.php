<?php

/* require the database connection file */
require_once 'connection.php';

// init Slim
require 'Slim/Slim.php';

$app = new Slim();

$conn = NULL;

// Api Rest code...
$app->post('/register-user', 'registerUser');
$app->get('/verify-email/:email/:verificationCode','verifyEmail');
$app->post('/Login', 'verifyLogin');
$app->post('/password-reset','resetPassword');
$app->post('/social-login','socialLogin');
$app->get('/validate-token','isValidToken');

$app->get('/projects/:user_id', 'getProjects');
$app->post('/add-project','addProject');
$app->post('/delete-project','deleteProject');
$app->post('/update-project','updateProject');

$app->post('/add-todo','addTodo');
$app->post('/delete-todo','deleteTodo');
$app->get('/get-todos/:id','getTodos');
$app->post('/mark-completed-todo','markCompletedTodo');
$app->post('/unmark-todo','unmarkTodo');

$app->post('/add-time-log','addTimeLog');
$app->get('/get-time-log/:project_id','getTodoTimeLogs');

$app->post('/add-message','addMessage');
$app->post('/send-mail','sendInviteMail');


$app->get('/get-known-user/:user_id','getKnownUsers');

$app->post('/add-member','addProjectMembers');
$app->get('/get-project-members/:project_id','getProjectMembers');


$app->get('/get-completed-todo/:project_id','getCompletedTodo');
$app->get('/get-project-todo/:project_id','getProjectTodo');




$app->run();



# getConnection
function getConnection(){
    global $conn;
    if(is_null($conn)){
        $conn = new Connection();
    }
    return $conn->getConnection();
}



/* function to register user */
function registerUser() {
    $request = Slim::getInstance()->request();

    $user = json_decode($request->getBody());

 /*   var_dump($user);*/

    $email_verification_code = generate_token();

    $epoch = time();


    $sql = "SELECT * FROM user WHERE email = '$user->email'";

    $db = getConnection();
    $stmt = $db->query($sql);
    $list = $stmt->fetch(PDO::FETCH_OBJ);
   //     $db = null;
        //	$access_token = generateAccessToken($list->id);
        //	$list->access_token = $access_token;
        if(!empty($list)){
            //echo "success";
            $user->error = "Email id is already registered with us";
            $user->status =  409 ;

            echo json_encode($user);
        }
        else {

            $sql1 = "INSERT INTO user(first_name, last_name, address, contact_no, email, username, password, organization_name, email_verification_code, created_on) VALUES (:first_name, :last_name, :address, :contact_no, :email, :username, :password, :organization_name, :email_verification_code, :created_on)";
            try {
             //   $db = getConnection();
                $stmt1 = $db->prepare($sql1);
                $stmt1->bindParam("first_name", $user->firstName);
                $stmt1->bindParam("last_name", $user->lastName);
                $stmt1->bindParam("address", $user->address);
                $stmt1->bindParam("contact_no", $user->contactNo);
                $stmt1->bindParam("email", $user->email);
                $stmt1->bindParam("username", $user->userName);
                $stmt1->bindParam("password", $user->password);
                $stmt1->bindParam("organization_name", $user->organization);
                $stmt1->bindParam("email_verification_code",$email_verification_code);
                $stmt1->bindParam("created_on",$epoch);
                $stmt1->execute();
                $user->id = $db->lastInsertId();

                if($user->id) {
                    sendMail($user->firstName, $user->email, $email_verification_code);
                }

                $user->status = 200;
                $user->message = "Registered Successfully";
                $db = null;
                echo json_encode($user);
            } catch(PDOException $e) {
                echo '{"error":{"text":'. $e->getMessage() .'}}';
            }

        }

}

/* end of function to register user */




/* function to generate token for email verification and as access token */
function generate_token() {
    $date = new DateTime();
    $timestamp =  $date->getTimestamp();

    //Generate a random string.
    $token = openssl_random_pseudo_bytes(16);

    //Convert the binary data into hexadecimal representation.
    $token = bin2hex($token).'-'.$timestamp;

    return $token;

}
/* end of function to generate email verification code */





/* function to send mail  */
function sendMail($name, $email, $email_verification_code) {

//	$message = "Your Activation Code is ".$code."";
    $to=$email;
    $subject="Simple Time Tracker Email Verification";
    $from = 'donotreply@gmail.com';

    //echo $name. '  '.$email;


    $url =  'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']).'/verify-email/'.$email . '/' . $email_verification_code;

    $body ='';

    $body='Hi ' .$name.', <br/><br/> Please click on the following link to verify your email <br/><br/> <a href="'.$url.'">'.$url.'</a> <br/><br/> to activate  your account.';

    echo $body;

    $headers = "From:".$from;
    $headers .= "Return-Path: ".$from."\r\n";
    $headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

    mail($to,$subject,$body,$headers);

}

/* end of function to send mail */






/* function to verify email */
function verifyEmail($email, $verificationCode){

    $timestamp2 = time();

    $sql = "select * FROM user WHERE email= '$email' AND email_verification_code = '$verificationCode'";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        if(!empty($list)){

            $timestamp1 = $list->created_on;

            $diff = abs($timestamp2 - $timestamp1) / 3600;
            $time_diff =  floor($diff);


            if($time_diff < 24) {

                $new_code = '';
                $email_verification= 1;

                $id= $list->id;

                //	echo "id =".$id;

                $sql = "UPDATE user SET email_verification_code =:email_verification_code ,email_verification =:email_verification WHERE id =:id ";
                try {
                    $db = getConnection();
                    $stmt = $db->prepare($sql);
                    $stmt->bindParam("email_verification_code", $new_code);
                    $stmt->bindParam("email_verification", $email_verification);
                    $stmt->bindParam("id", $id);
                    $stmt->execute();
                    $db = null;

                    echo "Email verified successfully!!!";


                } catch(PDOException $e) {
                    echo '{"error":{"text":'. $e->getMessage() .'}}';
                }


            }

        }
        else {
            echo '{"status":404,"msg":"invalid auth token"}';
        }
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

}
/* end of email verify function */




// Check login details
function verifyLogin() {
    $request = Slim::getInstance()->request();
    $user = json_decode($request->getBody());

    /*var_dump($user);*/
    //  print_r($user);
    $user_info =  (object)[];

    $sql = "select id, first_name, email, organization_name FROM user WHERE email ='".$user->username."' AND password = '".$user->password."'";

    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        //	$access_token = generateAccessToken($list->id);
        //	$list->access_token = $access_token;
        if(!empty($list)){

            $access_token = generateAccessToken($list->id);

            $user_info->status = 200;
            $user_info->messsage = 'Login Successful';
            $user_info->access_token = $access_token;
            $user_info->user = $list;

            //	print_r($list);
            echo json_encode($user_info);
        }
        else {
            echo '{"status":401,"msg":"invalid credentials"}';
        }

        //	echo json_encode($list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/*  end of function to verify login  */



/*  function to generate access token  */
function generateAccessToken($user_id){

   $token = generate_token();

    $sql = "UPDATE user SET access_token =:access_token WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("access_token", $token);
        $stmt->bindParam("id", $user_id);
        $stmt->execute();
        $db = null;
        return $token;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return '';
}
/* end of function to generate access token */


/* function to verify facebook login */
function socialLogin() {
    $request = Slim::getInstance()->request();
    $user = json_decode($request->getBody());

    $user_info =  (object)[];
    $sql = "select id, access_token, first_name, organization_name FROM user WHERE email ='".$user->email."'";

    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        //	$access_token = generateAccessToken($list->id);
        //	$list->access_token = $access_token;
        if(!empty($list)){
            //echo "success";

         //   $access_token = generate_token();
            /*
                    $user->name = $list->firstname;
                    $user->id = $list->id;
                    $user->email = $list->email;
                    $user->organization = $list->organization;*/
            $user_info->status = 200;
            $user_info->messsage = 'Login Successful';
            $user_info->access_token = $list->access_token;
            $user_info->user = $list;
            $user_info->user->email = $user->email;

            //	print_r($list);
            echo json_encode($user_info);
        }
        else {
            $epoch = time();

            $user->userName = substr($user->email, 0, strrpos($user->email, '@'));


            $email_verification = 1;

            $access_token = generate_token();

           // $sql1 = "INSERT INTO user(first_name, last_name, address, contact_no, email, username, password, organization_name, email_verification_code, created_on) VALUES (:first_name, :last_name, :address, :contact_no, :email, :username, :password, :organization_name, :email_verification_code, :created_on)";
            $sql1 = "INSERT INTO user(email, username, organization_name, email_verification, created_on, access_token) VALUES (:email, :username, :organization_name, :email_verification, :created_on, :access_token)";
            try {
                $db = getConnection();
                $stmt = $db->prepare($sql1);
               // $stmt->bindParam("first_name", $user->firstName);
              //  $stmt->bindParam("last_name", $user->lastName);
              //  $stmt->bindParam("address", $user->address);
             //   $stmt->bindParam("contact_no", $user->contactNo);
                $stmt->bindParam("email", $user->email);
                $stmt->bindParam("username", $user->userName);
          //      $stmt->bindParam("password", $user->password);
                $stmt->bindParam("organization_name", $user->name);
                $stmt->bindParam("email_verification",$email_verification);
                $stmt->bindParam("created_on",$epoch);
                $stmt->bindParam("access_token",$access_token);
                $stmt->execute();
                $user->id = $db->lastInsertId();

              /*  $user->status = 200;
                $user->message = "Login Successful";
                $db = null;
                echo json_encode($user);*/

                $user_info->status = 200;
                $user_info->messsage = 'Login Successful';
                $user_info->access_token = $access_token;
                $user_info->user = $user;


            } catch(PDOException $e) {
                echo '{"error":{"text":'. $e->getMessage() .'}}';
            }



        }

        //	echo json_encode($list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

}



/* function to get user data according to mail */
function resetPassword()
{
    $request = Slim::getInstance()->request();
    $user = json_decode($request->getBody());

    $email = $user->email;

    $sql = "SELECT * FROM user WHERE email = '$email'";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $user_info = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        /*	var_dump($user_info);*/

        if (!empty($user_info)) {
            $date = new DateTime();
            $timestamp = $date->getTimestamp();

            //Generate a random string.
            $reset_password_token = openssl_random_pseudo_bytes(16);
            //Convert the binary data into hexadecimal representation.
            $reset_password_token = bin2hex($reset_password_token) . '-' . $timestamp;

            $sql = "UPDATE user SET password_reset_token =:password_reset_token WHERE id=:id";
            try {
                $db = getConnection();
                $stmt = $db->prepare($sql);
                $stmt->bindParam("password_reset_token", $reset_password_token);
                $stmt->bindParam("id", $user_info->id);
                $stmt->execute();
                $db = null;

                sendPasswordResetMail($email, $user_info, $reset_password_token);

                $user->status = 200;
                $user->message = 'Please check your email to get password reset link';
                echo json_encode($user);


                //	return $token;
            } catch (PDOException $e) {
                echo '{"error":{"text":' . $e->getMessage() . '}}';
            }

        } else {
            $user->status = 401;
            $user->message = 'This email is not registered with us';
            echo json_encode($user);
        }

        //echo json_encode($user_info);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

/* end of function to get user data according to mail */


/* function to verify access token is valid or not */
function isValidToken() {
    $request =Slim::getInstance()->request();
    $id = $request->get('id');
    $token = $request->get('token');

    $user_info =  (object)[];

    $sql = "select id, first_name, email, organization_name FROM user WHERE id = ".$id." AND access_token = '$token'";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetch(PDO::FETCH_OBJ);
        $db = null;
        //$list->access_token = $access_token;
        if(!empty($list)){
            $user_info->status = 200;
            $user_info->messsage = 'Valid access token';
            $user_info->user = $list;

            //	print_r($list);
            echo json_encode($user_info);
        }
        else {
            echo '{"status":401,"msg":"invalid auth token"}';
        }
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to verify access token*/




/* function to send password reset mail */
function sendPasswordResetMail($email,$user_info,$reset_password_token) {
    $to=$email;
    $subject="Simple Time Tracker Password Reset";
    $from = 'donotreply@gmail.com';

    $url =  'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']).'/mail.php?password_reset_token='.$reset_password_token;


    //echo $url;
    $body ='';

    $body='Hi ' .$user_info->first_name.', <br/><br/> Please click on the following link to reset your password <br/><br/> <a href="'.$url.'">'.$url.'</a> <br/><br/> to activate  your account.';

//	echo $body;

    $headers = "From:".$from;
    $headers .= "Return-Path: ".$from."\r\n";
    $headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

    mail($to,$subject,$body,$headers);
}
/* end of function to send password reset mail */



/*  function to get all projects of particular user   */
function getProjects($user_id) {
    $sql = "SELECT * FROM project WHERE  owner_id = ".$user_id;
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $projects = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        if(!empty($projects)) {
            foreach($projects as $project) {
                $task = getTodos($project->id);
                $project->tasks = $task;
            }
        }
        echo json_encode($projects);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to get all projects of particular user   */



/* function to add project */
function addProject() {
    $request = Slim::getInstance()->request();
    $project = json_decode($request->getBody());

    // $date = date('Y-m-d H:i:s');

    $creation_time = time();
    $team_size = 1;

    $sql = "INSERT INTO project (owner_id, project_name, project_rate, project_icon, project_color, team_size, created_on) VALUES (:owner_id, :project_name, :project_rate, :project_icon, :project_color, :team_size, :created_on)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("owner_id", $project->userId);
        $stmt->bindParam("project_name", $project->projectName);
        $stmt->bindParam("project_rate", $project->projectPrice);
        $stmt->bindParam("project_icon", $project->projectIconName);
        $stmt->bindParam("project_color", $project->projectColorCode);
        $stmt->bindParam("team_size", $team_size);
        $stmt->bindParam("created_on", $creation_time);

        $stmt->execute();

        $project->id = $db->lastInsertId();


        if(!empty($project->tasks) && count($project->tasks) > 0){
              addProjectToDos($project->tasks,$project->id);
        }

        $project->status = 200;
        $project->messsage = 'Project Added Successfully';

        //var_dump($res);

        $db = null;
        echo json_encode($project);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to add project */


/* function to add to-dos to the project */
function addProjectToDos($project_id, $toDos) {
    $sql = "INSERT INTO to_do SET project_id =:project_id, text=:task_name";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);
        foreach($toDos as $d) {
            $stmt->execute(array(':project_id' => $project_id, ':text' => $d));
        }
        $db = null;

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

    return;
}
/* end of function to add to-dos*/


/* function to delete project */
function deleteProject() {
    $request = Slim::getInstance()->request();
    $project_data = json_decode($request->getBody());

//	var_dump($project_data);

    $sql = "DELETE FROM to_do WHERE project_id=:id";
    //var_dump($id);
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $project_data->id);
        $stmt->execute();


        $sql1 = "DELETE FROM project WHERE id=:id";
        //var_dump($id);
        $stmt1 = $db->prepare($sql1);
        $stmt1->bindParam("id", $project_data->id);
        $stmt1->execute();

        $project_data->status = 200;
        $project_data->message = 'Deleted Successfully';

        echo json_encode($project_data);

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

}
/* end of function to delete project */


/* function to update project name */
function updateProject() {
    $request = Slim::getInstance()->request();
    $project_data = json_decode($request->getBody());

    $sql = "UPDATE project SET project_name =:project_name WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("project_name", $project_data->project_name);
        $stmt->bindParam("id", $project_data->project_id);
        $stmt->execute();
        $db = null;

        $project_data->status=200;
        $project_data->message= 'Project Updation Successful';
        echo json_encode($project_data);

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    
}
/* end of function to update project name */


/* function to add single todo */
function addTodo() {
    $request = Slim::getInstance()->request();
    $todo = json_decode($request->getBody());

    $created_on = time();
    $status =0;

    $sql = "INSERT INTO to_do SET project_id =:project_id, text =:text, created_on =:created_on, status =:status";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("project_id", $todo->project_id);
        $stmt->bindParam("text",  $todo->task_name);
        $stmt->bindParam("created_on",  $created_on);
        $stmt->bindParam("status",  $status);

        $stmt->execute();
        $todo->id = $db->lastInsertId();
        $todo->status = 200;
        $todo->message = "Task Added Successfully";
        echo json_encode($todo);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of funtion to add single todo */


/* function to delete todo */
function deleteTodo(){
    $request = Slim::getInstance()->request();
    $todo_data = json_decode($request->getBody());

//	var_dump($project_data);

    $sql = "DELETE FROM to_do WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $todo_data->id);
        $stmt->execute();

        $todo_data->status = 200;
        $todo_data->message = 'Todo Deleted Successfully';

        echo json_encode($todo_data);

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to delete todo */


/* function to add members to the project */
function addProjectMembers() {
    $request = Slim::getInstance()->request();
    $member = json_decode($request->getBody());

    $created_time = time();

    $sql = "INSERT INTO project_member_table SET project_id =:project_id, user_id =:user_id, created_time =:created_time, member_permissions =:member_permissions, user_role =:user_role";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("project_id", $member->project_id);
        $stmt->bindParam("user_id",  $member->user_id);
        $stmt->bindParam("created_time", $created_time);
        $stmt->bindParam("member_permissions",  $member->member_permissions);
        $stmt->bindParam("user_role",  $member->user_role);
        $stmt->execute();
        $member->id = $db->lastInsertId();
        $member->status = 200;
        $member->message = "Member Added Successfully";
        echo json_encode($member);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to add members  */


/* function to add time log */
function addTimeLog() {
    $request = Slim::getInstance()->request();
    $log_data = json_decode($request->getBody());

//	var_dump($task_data);

    $date = $log_data->date;
    $date = str_replace('/', '-', $date);
    $date = date('Y-m-d', strtotime($date));
  //  $end_date =  $log_data->date;

    $sql = "INSERT INTO task_time_log SET todo_id =:todo_id, start_date =:start_date, end_date =:end_date, start_time =:start_time, end_time =:end_time, time_diff_mins =:time_diff_mins, memo =:memo, member_id =:member_id";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("todo_id", $log_data->task);
        $stmt->bindParam("start_date",  $date);
        $stmt->bindParam("end_date",  $date);
        $stmt->bindParam("start_time",  $log_data->start_time);
        $stmt->bindParam("end_time",  $log_data->end_time);
        $stmt->bindParam("time_diff_minutes", $log_data->time_diff_minutes);
        $stmt->bindParam("memo",  $log_data->memo);
        $stmt->bindParam("member_id",  $log_data->member_id);

        $stmt->execute();
        $log_data->id = $db->lastInsertId();
        $log_data->status = 200;
        $log_data->message = "Time Log Added Successfully";
        echo json_encode($log_data);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to add time log */


/* function to add messages */
function addMessage() {
    $request = Slim::getInstance()->request();
    $message = json_decode($request->getBody());


    $sql = "INSERT INTO message SET from_user =:from_user, to_user =:to_user, text =:text, message_from =:message_from, message_source_id =:message_source_id";
    try {

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("from_user", $message->from_user);
        $stmt->bindParam("to_user",  $message->to_user);
        $stmt->bindParam("text",  $message->text);
        $stmt->bindParam("message_from",  $message->message_from);
        $stmt->bindParam("message_source_id",  $message->message_source_id);

        $stmt->execute();
        $message->id = $db->lastInsertId();
        $message->status = 200;
        $message->message = "Message Added Successfully";
        echo json_encode($message);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to add messages */


/* function to get data for todo time logs */
function getTodoTimeLogs($project_id) {
    /*var_dump($project_id);*/

    // $sql = "select id as task_id,task_name FROM task WHERE project_id=".$id." ORDER BY id";
    $sql = "SELECT t.id, t.text, t.project_id, tl.start_time, tl.end_time, tl.start_date, tl.time_diff_mins FROM task_time_log AS tl INNER JOIN to_do AS t ON tl.todo_id = t.id WHERE t.project_id =".$project_id;
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        //return($list);
        //	$list->status = 200;
        echo json_encode($list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

}
/* end of function to get todo time logs */


/* function to get project todos */
function getTodos($project_id) {
    $sql = "select id as todo_id, text FROM to_do WHERE project_id =".$project_id." AND status = 0 ORDER BY id";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        //	$list = $stmt->fetch(PDO::FETCH_OBJ);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        return $list;
        //	echo json_encode($list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

    return;
}
/* end of project to get todos */




/* function to get known users list */
function getKnownUsers($user_id) {
    //$sql = "SELECT * FROM known_user WHERE  user_id = ".$user_id;
//    $sql = "SELECT u.first_name, u.last_name, u.email FROM USER AS u INNER JOIN known_user AS ku ON u.id = ku.id WHERE ku.user_id = ".$user_id;
//    $sql = "SELECT u.first_name, u.last_name, u.email FROM user AS u INNER JOIN known_user AS ku  ON u.id = ku.id WHERE ku.user_id = ".$user_id;
  $sql =  "SELECT u.first_name, u.last_name, u.email FROM user AS u INNER JOIN known_user AS ku  ON u.id = ku.known_user_id WHERE user_id = ".$user_id;
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $user_list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($user_list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to get known users list */


/* function to send invite mail to members */
function sendInviteMail() {
    $request = Slim::getInstance()->request();
    $members = json_decode($request->getBody());

   foreach($members as $member) {
        $to= $member->email;
        $subject="Simple Time Tracker Project Membership Invitation";
        $from = 'donotreply@gmail.com';

        $body ='';

        //  $body='Hi ' .$name.', <br/><br/> Please click on the following link to verify your email <br/><br/> <a href="'.$url.'">'.$url.'</a> <br/><br/> to activate  your account.';

        $accept_url =  'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']).'/accept-invite/'.$member->email;
        $reject_url =  'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']).'/reject-invite/'.$member->email;

        $body = 'Hi ,<br/><br/> Click accept to accept the project membership or reject to reject it. <br/><br/> <a  style="color:#fff;background:green;padding:6px 12px;border:0;border-radius:2px;margin-right:20px;" href="'.$accept_url.'">Accept</a><a style="color:#fff;background:red;padding:6px 12px;border:0;border-radius:2px;" href="'.$reject_url.'">Reject</a>';

        echo $body;

        $headers = "From:".$from;
        $headers .= "Return-Path: ".$from."\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        mail($to,$subject,$body,$headers);
   }


}
/* end of function to send mail to members*/


/* function to get project members list */
function getProjectMembers($project_id) {
    $sql = "SELECT u.first_name, u.last_name, u.email FROM user AS u INNER JOIN project_member_table AS pm  ON u.id = pm.user_id WHERE pm.project_id = ".$project_id;
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $member_list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($member_list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to get project members list */



/* function to set the task as completed */
function markCompletedTodo() {
    $request = Slim::getInstance()->request();
    $todo = json_decode($request->getBody());

    $completed_timestamp = time();
    $status = 1;

    $sql = "UPDATE to_do SET status =:status, completed_timestamp =:completed_timestamp WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("status", $status);
        $stmt->bindParam("completed_timestamp", $completed_timestamp);
        $stmt->bindParam("id", $todo->id);
        $stmt->execute();
        $db = null;

        $todo->status = 200;
        $todo->message = 'Todo Completed';
        echo json_encode($todo);

        //	return $token;
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }

}
/* end of function to set the task as completed */


/* function to get completed todo */
function getCompletedTodo($project_id) {
    $sql = "select * FROM to_do WHERE project_id =".$project_id." AND status = 1 ORDER BY id";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        //	$list = $stmt->fetch(PDO::FETCH_OBJ);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
    //    return $list;
        echo json_encode($list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
/* end of function to get completed todo */


/*function to get project todo*/
function getProjectTodo($project_id) {
    $sql = "select id as todo_id, text FROM to_do WHERE project_id =".$project_id." AND status = 0 ORDER BY id";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        //	$list = $stmt->fetch(PDO::FETCH_OBJ);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($list);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }

}
/* end of function to get project todo */


/* function to unmark todo */
function unmarkTodo() {
    $request = Slim::getInstance()->request();
    $todo = json_decode($request->getBody());

    $status = 0;

    $sql = "UPDATE to_do SET status =:status WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("status", $status);
        $stmt->bindParam("id", $todo->id);
        $stmt->execute();
        $db = null;

        $todo->status = 200;
        $todo->message = 'Todo Incomplete';
        echo json_encode($todo);

        //	return $token;
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }

}
/* end of function to unmark todo */
