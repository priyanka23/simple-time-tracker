app.directive('comparePassword',function(){

 //   console.log('abc');

    return {
        require: 'ngModel',
        // replace: true,
        restrict: 'A',

        link:function (scope, element, attrs, ctrl) {
            
            ctrl.$parsers.unshift(checkPassword);

            function checkPassword(viewValue){
                //console.log('view',viewValue);
                //console.log('val of pass',scope.registerForm.password.$viewValue);
                var passMatch = viewValue === scope.registerForm.password.$viewValue;

                ctrl.$setValidity('passMatch', passMatch);
                return viewValue;

            }
        }
    };

});