var app = angular.module('timeTrackerApp', ['ngRoute','ngCookies','ngMessages','oc.lazyLoad']);

app.config(['$ocLazyLoadProvider','$routeProvider', function($ocLazyLoadProvider, $routeProvider ) {
    $routeProvider
        // .when('/',  {
        //     templateUrl: 'views/login.html',
        //     controller: 'LoginController'
        // })

        .when('/', {
            title: 'Login',
            templateUrl: 'views/login.html',
            controller: 'LoginController',
            controllerAs: 'login_ctrl',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(['controllers/LoginController.js',
                        'controllers/SideNavController.js'
                       /* 'controllers/HeaderController.js',
                        'assets/directive/angular-pageslide-directive.js',
                        'assets/js/jstz-1.0.4.min.js',
                        'assets/js/momentjs/angular-jstz.js',
                        'assets/directive/ngOnTheFlyCurrency.js',
                        // 'assets/js/bower_components/angular-sanitize.js',
                        'assets/js/bower_components/ngNotificationsBar.min.js',
                        'assets/js/angularjs-social-login.js',*/


                    ]);
                }]
            }
        })

        .when('/project', {
            title: 'Project',
            templateUrl: 'views/project.html',
            controller: 'ProjectController',
            controllerAs: 'project',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(['controllers/ProjectController.js',
                        'controllers/SideNavController.js',
                        'services/utility.js',
                        'filters/formatTimer.js'
                     /*    'assets/js/angularjs-social-login.js',*/


                    ]);
                }]
            }
        })



       /* .when('/project',  {
            templateUrl: 'views/project.html',
            controller: 'ProjectController'
        })*/


        .when('/client', {
            title: 'Client',
            templateUrl: 'views/client.html',
            controller: 'ClientController',
            controllerAs: 'client',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(['controllers/ClientController.js',
                        'controllers/SideNavController.js',
                        'assets/lib/angular-color-this.js',
                        'filters/firstChar.js'
                        /*  'assets/directive/angular-pageslide-directive.js',
                         'assets/js/jstz-1.0.4.min.js',
                         'assets/js/momentjs/angular-jstz.js',
                         'assets/directive/ngOnTheFlyCurrency.js',
                         // 'assets/js/bower_components/angular-sanitize.js',
                         'assets/js/bower_components/ngNotificationsBar.min.js',
                         'assets/js/angularjs-social-login.js',*/


                    ]);
                }]
            }
        })

/*

        .when('/client',  {
            templateUrl: 'views/client.html',
            controller: 'ClientController'
        })
*/

        .when('/projects',  {
            templateUrl: 'views/all_project.html',
            controller: 'ProjectListController'
        })

        .when('/manual_entry', {
            title: 'Manual Entry',
            templateUrl: 'views/manual_entry.html',
            controller: 'ManualEntryController',
            controllerAs: 'manual_entry',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(['controllers/ManualEntryController.js',
                        'controllers/SideNavController.js',
                        'assets/materialize_clockpicker/materialize.clockpicker.js'
                        /*  'assets/directive/angular-pageslide-directive.js',
                         'assets/js/jstz-1.0.4.min.js',
                         'assets/js/momentjs/angular-jstz.js',
                         'assets/directive/ngOnTheFlyCurrency.js',
                         // 'assets/js/bower_components/angular-sanitize.js',
                         'assets/js/bower_components/ngNotificationsBar.min.js',
                         'assets/js/angularjs-social-login.js',*/


                    ]);
                }]
            }
        })

       /*
        .when('/manual_entry',  {
            templateUrl: 'views/manual_entry.html',
            controller: 'ManualEntryController'
        })*/

        .when('/manage_project', {
            title: 'Manage Project',
            templateUrl: 'views/manage_project.html',
            controller: 'ManageProjectController',
            controllerAs: 'manage_project',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(['controllers/ManageProjectController.js',
                            'controllers/SideNavController.js',
                            'assets/sweetAlert/sweet-alert.min.js',
                            'assets/sweetAlert/SweetAlert.min.js',
                            'services/utility.js'
                        /*    'assets/js/momentjs/angular-jstz.js',
                         'assets/directive/ngOnTheFlyCurrency.js',
                         // 'assets/js/bower_components/angular-sanitize.js',
                         'assets/js/bower_components/ngNotificationsBar.min.js',
                         'assets/js/angularjs-social-login.js',*/


                    ]);
                }]
            }
        })


        .when('/review_entry', {
            title: 'Review Entries',
            templateUrl: 'views/review_entry.html',
            controller: 'ReviewEntryController',
            controllerAs: 'review_entry',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(['controllers/ReviewEntryController.js',
                        'controllers/SideNavController.js',
                        'filters/getFormattedTime.js',
                        'filters/formatDate.js',
                        'filters/minsHours.js'
                        /*    'assets/directive/ngOnTheFlyCurrency.js',
                         // 'assets/js/bower_components/angular-sanitize.js',
                         'assets/js/bower_components/ngNotificationsBar.min.js',
                         'assets/js/angularjs-social-login.js',*/


                    ]);
                }]
            }
        })


        .when('/logout', {
            title: 'Logout',
            templateUrl: 'views/login.html',
            controller: 'LogoutController',
            controllerAs: 'logout',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(['controllers/LogoutController.js',
                        'controllers/SideNavController.js']);
                }]
            }
        })



        .otherwise({redirectTo: '/'})

}])


    .run(['$rootScope', '$location', '$cookies', '$http', '$route',   function ($rootScope, $location, $cookies, $http, $route) {


        $rootScope.pageTitle = "Login";

        $rootScope.$on("$locationChangeStart", function (event, next, current)
        {
            var userCookie = $cookies.get('user');

          //  console.log('user cookie',userCookie);
            $rootScope.authenticated = false;
            //  console.log($location.path());
            if(angular.isUndefined(userCookie) !== true &&  userCookie !== null) {

                console.log('user cookie',JSON.parse($cookies.get('user')));


                $rootScope.authenticated = JSON.parse($cookies.get('user'));
                //   console.log('accss',$rootScope.authenticated.access_token);
                    console.log('id',$rootScope.authenticated.user.id);

                $http.get('api/validate-token?token=' + $rootScope.authenticated.access_token + '&id=' + $rootScope.authenticated.user.id).then(function (result) {
                    console.log('res', result);
                    if (result.data.status == 200) {
                        //  console.log($rootScope.authenticated.access_token);
                     //   $rootScope.authenticated = true;

                       console.log('user auth',$rootScope.authenticated);

                    }else {
                        $cookies.remove('user');
                        $rootScope.authenticated = false;
                        $location.path('/login');
                    }
                });
            }
            else if ($location.path() == '/'  ||  $location.path() == '') {

            }
            else {

                $rootScope.authenticated = false;
                console.log('path',$location.path());
                $location.path('/login');
                // location.href ='/';

            }
        });

            $rootScope.$on("$locationChangeSuccess", function (event, current, previous) {

                 $rootScope.pageTitle = $route.current.title;
            });

    }]);






